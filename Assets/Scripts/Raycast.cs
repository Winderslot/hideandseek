using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycast : MonoBehaviour
{
    public float HP;
    public GameObject PlaceToTP;
    bool inObject = false;
    //public Transform rightPos;
    public bool untarget = false;
    RaycastHit hit;
    public float rayDistance = 4;
    public GameObject ghostMesh;
    // Start is called before the first frame update
    void Start()
    {
        HP = 12;
        
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Physics.Raycast(ray, out hit, rayDistance);
        if (Input.GetMouseButtonDown(0))
        {
            if (hit.collider)
            {
                if (hit.collider.gameObject.tag == "special")
                {
                    Settle();
                }
            }
        }
        if (inObject == true)
        {
            if (Input.GetMouseButton(1))
            {
                inObject = false;
                Out();
            }
        }
        if (HP <= 0)
        {
            Destroy(gameObject);
        }
    }
    public void Settle()
    {
        inObject = true;
        //gameObject.GetComponentInChildren<ParticleSystem>().gameObject.SetActive(false); 
        PlaceToTP.transform.position = gameObject.transform.position;
        gameObject.GetComponent<CapsuleCollider>().enabled = false;
        gameObject.GetComponent<Rigidbody>().isKinematic = true;
        gameObject.transform.position = hit.collider.gameObject.transform.position;
        ghostMesh.GetComponent<MeshRenderer>().enabled = false;
        
        //������ ����� ��� �������� ���            
    }      
    public void Out()
    {
        gameObject.transform.position = PlaceToTP.transform.position;
        //gameObject.transform.position = rightPos.position;
        gameObject.GetComponent<CapsuleCollider>().enabled = true;       
        gameObject.GetComponent<Rigidbody>().isKinematic = false;
        ghostMesh.GetComponent<MeshRenderer>().enabled = true;
    }
}

