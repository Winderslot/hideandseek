using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HunterScript : MonoBehaviour
{
    RaycastHit hit;
    public float rayDistance = 4;
    GameObject enemy;
    public float enemyHP;
    public Material enemyMaterial;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Physics.Raycast(ray, out hit, rayDistance);
        if (hit.collider)
        {
            if (hit.collider.gameObject.tag == "Ghost")
            {
                enemy = hit.collider.gameObject;
                enemyMaterial = hit.collider.gameObject.GetComponent<Material>();
            }        
        }
        if (Input.GetMouseButton(0))
        {
            if (hit.collider)
            {
                if(hit.collider.gameObject == enemy)
                {
                    enemyHP = enemy.GetComponent<Raycast>().HP;
                    MakeDamage();
                }
            }
        }       
    }
    public void MakeDamage()
    {
        enemyHP = enemyHP - 5 * Time.deltaTime;
        enemy.GetComponent<Raycast>().HP = enemyHP;
        
    }
}
